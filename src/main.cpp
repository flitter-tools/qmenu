#include "mainwindow.h"
#include <QApplication>
#include <QObject>
#include <QCommandLineParser>
#include "third-party/SingleApplication/singleapplication.h"

int main(int argc, char *argv[])
{
    SingleApplication app(argc, argv, true, SingleApplication::Mode::SecondaryNotification);

    Mode mode = Mode::DMENU;

    if (app.arguments().count() > 0)
    {
        QCommandLineParser parser;
        parser.setApplicationDescription(QStringLiteral("QMenu -- dmenu like application using Qt."));
        parser.addHelpOption();
        parser.addVersionOption();
        parser.addOption(QCommandLineOption({QStringLiteral("a"), QStringLiteral("applications")},
                                            QStringLiteral("Applications mode.")));
        parser.process(app.arguments());

        if (parser.isSet("a") || parser.isSet("applications"))
            mode = Mode::APPLICATIONS;
    }

    if(app.isSecondary())
    {
        if (app.arguments().count() > 0)
            app.sendMessage(QByteArray::number(mode));
        return 0;
    }

    MainWindow w;
    w.switchMode(mode);
    QObject::connect(&app, &SingleApplication::instanceStarted, &w, &MainWindow::show);
    QObject::connect(&app, &SingleApplication::receivedMessage, &w, &MainWindow::getMessage);

    return app.exec();
}
