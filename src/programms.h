#ifndef PROGRAMMS_H
#define PROGRAMMS_H

#include <QStringList>

class Programms
{
    public:
        Programms();
        ~Programms(){}

        QStringList getBin() const;

    private:
        void parseBin();
        QStringList m_bin;
};

#endif // PROGRAMMS_H
