#include "programms.h"

#include <QByteArray>
#include <QFileInfo>
#include <QDirIterator>
#include <QDir>

Programms::Programms()
{
    parseBin();
}

QStringList Programms::getBin() const
{
    return m_bin;
}

void Programms::parseBin()
{
    const QString &path = QString::fromLatin1(getenv("PATH"));

    QStringList pathArray = path.split(':');
    pathArray.removeDuplicates();

    for (const QString &p : pathArray)
    {
        QDirIterator it(p);

        while(it.hasNext())
        {
            const QString &file = it.next();
            const QString &realFile = QFileInfo(file).baseName();
            if (!realFile.isEmpty())
            {
                m_bin.push_back(realFile);
            }
        }
    }  
}
