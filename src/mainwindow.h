#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>

namespace Ui {
class MainWindow;
}

enum Mode
{
    DMENU = 0,
    APPLICATIONS
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = nullptr);
        ~MainWindow();

        void switchMode(Mode mode);

    public slots:
        void getMessage(quint32 instanceId, QByteArray message);

    private slots:
        void on_searchField_textChanged(const QString &arg1);
        void on_programList_itemClicked(QListWidgetItem *item);
        void on_applicationsList_itemClicked(QListWidgetItem *item);
        void on_switchTabVisibility_clicked();

    protected:
        bool eventFilter(QObject *obj, QEvent *ev);
        void closeEvent(QCloseEvent *event);

    private:
        Ui::MainWindow *ui;
        void selectItem(int pos);
        void selectDmenuItem(int pos);
        void selectApplicationItem(int pos);
        void searchDmenu(const QString &arg1);
        void searchApplication(const QString &arg1);
        void tabSwitchVisibility();

        int currentSelection;
        QList<int> selectionIndexes;
};

#endif // MAINWINDOW_H
