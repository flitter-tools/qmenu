#include "cutemime.h"

#include "ftlip/QT/ftlip.h"

#include <QDebug>
#include <QDir>
#include <QDirIterator>
#include <QTextStream>
#include <QFile>

QMap<QString, QStringList> CuteMime::getMimeList() const
{
    QMap<QString, QStringList> ret;

    QFile inputFile(QDir::homePath() + "/.local/share/applications/mimeapps.list");

    if (inputFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&inputFile);

        while (!in.atEnd())
        {
            QString line = in.readLine();

            if (line[0] == '[' || line[0] == '#' || line.isEmpty())
                continue;

            ret[line.split("=")[0]] = line.split("=")[1].split(";");
        }

        inputFile.close();
    }

    return ret;
}

Desktop CuteMime::parseDesktopFile(const QString &filePath, QString region) const
{
    ftlip desktop_ini(filePath);

    if (!region.isEmpty())
        region = "[" + region + "]";

    struct Desktop desktop;

    desktop.exec = desktop_ini.get("Exec=");
    desktop.name = desktop_ini.get("Name" + region + "=");
    desktop.comment = desktop_ini.get("Comment" + region + "=");
    desktop.tryExec = desktop_ini.get("TryExec=");
    desktop.terminal = (desktop_ini.get("Terminal") == "false" ? true : false);
    desktop.noDisplay = (desktop_ini.get("NoDisplay") == "false" ? true : false);
    desktop.genericName = desktop_ini.get("GenericName" + region + "=");
    desktop.filePath = filePath;

    QString iconName = desktop_ini.get("Icon=", "preferences-activities");
    desktop.icon = getThemeIcon(iconName);

    const QString &mimetype = desktop_ini.get("MimeType=");

    if (!mimetype.isEmpty() && !mimetype.isNull())
        desktop.mimeType = mimetype.split(";");

    const QString &actions = desktop_ini.get("Actions=");

    if (!actions.isEmpty() && !actions.isNull())
        desktop.actions = actions.split(";");

    const QString &categories = desktop_ini.get("Categories=");

    if (!categories.isEmpty() && !categories.isNull())
        desktop.categories = categories.split(";");

    return desktop;
}

Desktop CuteMime::findDesktopFile(const QString &file) const
{
    const QStringList &directories = {
       QDir::homePath() + "/.local/share/applications/",
        "/usr/share/applications/"
    };

    for (const QString &p : directories)
    {
        if (QDir(p + file + ".desktop").exists())
            return parseDesktopFile(p + file + ".desktop");
    }

    return Desktop();
}

QList<Desktop> CuteMime::getListDesktopFiles(const QString &region) const
{
    const QStringList &directories = {
       QDir::homePath() + "/.local/share/applications/",
        "/usr/share/applications"
    };

    QList<Desktop> ret;

    for (const QString &path : directories)
    {
        QDirIterator it(path, QStringList() << "*.desktop");

        while (it.hasNext())
        {
            struct Desktop desktop = parseDesktopFile(it.next(), region);
            if (!desktop.noDisplay)
                ret.push_back(desktop);
        }
    }

    return ret;
}

QIcon CuteMime::getThemeIcon(QString iconName) const
{
    // standart allows to do this shit...
    iconName.remove(".png");
    iconName.remove(".xpm");

    QIcon icon;
    icon = QIcon::fromTheme(iconName);

    if (icon.isNull())
    {
        QFile filePng("/usr/share/pixmaps/" + iconName + ".png");
        QFile fileXpm("/usr/share/pixmaps/" + iconName + ".xpm");

        if (filePng.exists())
        {
            icon = QIcon(filePng.fileName());
        }
        else if (fileXpm.exists())
        {
            icon = QIcon(fileXpm.fileName());
        }
        else
        {
            icon = getHicolorIcon(iconName);

            // nothing works...
            if (icon.isNull())
                icon = QIcon::fromTheme("preferences-activities");
        }
    }

    return icon;
}

QIcon CuteMime::getHicolorIcon(const QString &iconName) const
{
    const QStringList &sizeList ={
        "16", "20", "22", "24", "32", "36", "40",
        "48", "64", "72", "96", "128", "192",
        "256", "480", "512"
    };

    for (const QString &size : sizeList)
    {
        const QString &path("/usr/share/icons/hicolor/" + size + "x" + size + "/apps/" + iconName);

        if (QFile(path + ".png").exists())
            return QIcon(path + ".png");
        else if (QFile(path + ".xpm").exists())
            return QIcon(path + ".xpm");
    }

    return QIcon();
}
