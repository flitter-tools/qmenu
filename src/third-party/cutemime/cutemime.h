#ifndef CUTEMIME_H
#define CUTEMIME_H

#include <QString>
#include <QIcon>
#include <QStringList>
#include <QMap>

struct DesktopAction
{
    QString actionName;
    QString name;
    QString exec;
};

struct Desktop
{
    QString name;
    QString genericName;
    QString exec;
    QString tryExec;
    QIcon icon;
    QStringList categories;
    QStringList actions;
    QString comment;
    bool terminal;
    bool noDisplay;
    QStringList mimeType;
    //QList<DesktopAction> actionsList; // stub

    QString filePath;
};

class CuteMime
{
    public:
        CuteMime(){}
        ~CuteMime(){}

        QMap<QString, QStringList> getMimeList() const;
        Desktop parseDesktopFile(const QString &filePath, QString region = "") const;
        Desktop findDesktopFile(const QString &file) const;
        QList<Desktop> getListDesktopFiles(const QString &region = "") const;

    private:
        QIcon getHicolorIcon(const QString &iconName) const;
        QIcon getThemeIcon(QString iconName) const;
};

#endif // CUTEMIME_H
