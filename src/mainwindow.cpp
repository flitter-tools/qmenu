#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QProcess>
#include <QKeyEvent>
#include <QGuiApplication>
#include <QScreen>
#include <QDebug>

#include "programms.h"
#include "third-party/cutemime/cutemime.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    currentSelection(-1)
{
    ui->setupUi(this);

    int screenW = QGuiApplication::screens().first()->size().width();
    int widgetWidth = ui->searchField->minimumWidth();
    setGeometry(screenW/2-widgetWidth/2, 50, geometry().width(), geometry().height());

    ui->searchField->installEventFilter(this);
    this->layout()->setSizeConstraint(QLayout::SetFixedSize);

    for (const QString &item : Programms().getBin())
    {
        QListWidgetItem *witem = new QListWidgetItem(ui->programList);
        witem->setSizeHint(QSize(witem->sizeHint().width(), 30));
        witem->setText(item);
        ui->programList->addItem(witem);
    }

    for (const Desktop &desktop : CuteMime().getListDesktopFiles())
    {
        QListWidgetItem *witem = new QListWidgetItem(ui->applicationsList);
        witem->setSizeHint(QSize(witem->sizeHint().width(), 30));
        witem->setText(desktop.name);
        witem->setIcon(desktop.icon);
        witem->setData(Qt::UserRole, desktop.exec);
        ui->programList->addItem(witem);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::getMessage(quint32 instanceId, QByteArray message)
{
    Q_UNUSED(instanceId)

    if (message.toInt() == Mode::DMENU)
        switchMode(Mode::DMENU);
    else
        switchMode(Mode::APPLICATIONS);
}

void MainWindow::switchMode(Mode mode)
{
    ui->tabWidget->setCurrentIndex(mode);
    if (mode == Mode::APPLICATIONS)
    {
        ui->iconSearch->setPixmap(QIcon::fromTheme("search").pixmap(25));
        ui->switchTabVisibility->setHidden(true);
        ui->tabWidget->setHidden(false);
    }
    else
    {
        ui->iconSearch->setPixmap(QIcon::fromTheme("arrow-right").pixmap(25));
        ui->switchTabVisibility->setHidden(false);
        ui->tabWidget->setHidden(true);
    }
}

void MainWindow::tabSwitchVisibility()
{
    bool isHidden = ui->tabWidget->isHidden();
    ui->tabWidget->setHidden(!isHidden);

    if (!isHidden)
        ui->switchTabVisibility->setIcon(QIcon::fromTheme("arrow-down"));
    else
        ui->switchTabVisibility->setIcon(QIcon::fromTheme("arrow-up"));
}

bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{
    if (obj == ui->searchField)
    {
        if (event->type() == QEvent::KeyPress)
        {
            QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);

            switch (keyEvent->key())
            {
                case Qt::Key_Return:
                case Qt::Key_Enter:
                    if (ui->tabWidget->currentIndex() == Mode::APPLICATIONS && ui->applicationsList->currentItem() != nullptr)
                        QProcess::startDetached(ui->applicationsList->currentItem()->data(Qt::UserRole).toString());
                    else if (ui->tabWidget->currentIndex() == Mode::DMENU && ui->programList->currentItem() != nullptr)
                        QProcess::startDetached(ui->programList->currentItem()->text());
                    else
                        QProcess::startDetached(ui->searchField->text());
                    this->hide();
                    break;

                case Qt::Key_Down:
                    selectItem(Qt::Key_Down);
                    break;

                case Qt::Key_Up:
                    selectItem(Qt::Key_Up);
                    break;

                case Qt::Key_PageDown:
                    selectItem(Qt::Key_PageDown);
                    break;

                case Qt::Key_PageUp:
                    selectItem(Qt::Key_PageUp);
                    break;

                case Qt::Key_Escape:
                    this->hide();
                    break;

                default:
                    return QMainWindow::eventFilter(obj, event);
            }
        }
        else if (event->type() == QEvent::FocusOut)
        {
            ui->searchField->setFocus();
            return true;
        }
    }
    else
    {
        // pass the event on to the parent class
        return QMainWindow::eventFilter(obj, event);
    }

    return false;
}

void MainWindow::selectDmenuItem(int pos)
{
    if (ui->tabWidget->isHidden())
    {
        ui->tabWidget->setHidden(false);
        ui->switchTabVisibility->setIcon(QIcon::fromTheme("arrow-up"));
    }

    if (selectionIndexes.isEmpty())
        for (int i = 0; i < ui->programList->count(); ++i)
            selectionIndexes.push_back(i);

    ui->programList->clearSelection();

    int index = 0;

    if (pos == Qt::Key_Down)
    {
        index = selectionIndexes[currentSelection+1];
        currentSelection += 1;
    }
    else if (pos == Qt::Key_Up)
    {
        index = selectionIndexes[currentSelection-1];
        currentSelection -= 1;
    }
    else if (pos == Qt::Key_PageDown)
    {
        index = selectionIndexes[currentSelection+5];
        currentSelection += 5;
    }
    else if (pos == Qt::Key_PageUp)
    {
        index = selectionIndexes[currentSelection-5];
        currentSelection -= 5;
    }

    //if (index < 0)
    //    index = items[0];
    //else if (index > items.count()-1)
    //    index = items[items.count()]

    ui->programList->setCurrentRow(index);
}

void MainWindow::selectApplicationItem(int pos)
{
    if (ui->applicationsList->currentRow() == -1)
    {
        ui->applicationsList->setCurrentRow(0);
    }
    else
    {
        if (pos == Qt::Key_Up)
            ui->applicationsList->setCurrentRow(ui->applicationsList->currentRow()-1);
        else if (pos == Qt::Key_Down)
            ui->applicationsList->setCurrentRow(ui->applicationsList->currentRow()+1);
        else if (pos == Qt::Key_PageUp)
            ui->applicationsList->setCurrentRow(ui->applicationsList->currentRow()-5);
        else if (pos == Qt::Key_PageDown)
            ui->applicationsList->setCurrentRow(ui->applicationsList->currentRow()+5);
    }
}

void MainWindow::selectItem(int pos)
{
    if (ui->tabWidget->currentIndex() == Mode::DMENU)
        selectDmenuItem(pos);
    else
        selectApplicationItem(pos);
}

void MainWindow::searchDmenu(const QString &arg1)
{
    currentSelection = -1;
    selectionIndexes.clear();

    if (!arg1.isEmpty())
    {
        for (int i = 0; i < ui->programList->count(); ++i)
        {
            if (!ui->programList->item(i)->text().contains(arg1, Qt::CaseInsensitive))
            {
                ui->programList->item(i)->setHidden(true);
                continue;
            }

            selectionIndexes.push_back(i);
        }

        if (selectionIndexes.count() == 0)
        {
            ui->tabWidget->setHidden(true);
            ui->switchTabVisibility->setIcon(QIcon::fromTheme("arrow-down"));
        }
        else
        {
            ui->tabWidget->setHidden(false);
            ui->switchTabVisibility->setIcon(QIcon::fromTheme("arrow-up"));
        }
    }
    else
    {
        for (int i = 0; i < ui->programList->count(); ++i)
        {
            ui->programList->item(i)->setHidden(false);
        }

        tabSwitchVisibility();
    }
}

void MainWindow::searchApplication(const QString &arg1)
{
    if (!arg1.isEmpty())
    {
        for (int i = 0; i < ui->applicationsList->count(); ++i)
        {
            if (ui->applicationsList->item(i)->text().contains(arg1, Qt::CaseInsensitive))
            {
                ui->applicationsList->item(i)->setSelected(true);
                ui->applicationsList->scrollToItem(ui->applicationsList->item(i));
                ui->applicationsList->setCurrentRow(i);
            }
        }
    }
    else
    {
        ui->applicationsList->clearSelection();
    }
}

void MainWindow::on_searchField_textChanged(const QString &arg1)
{
    if (ui->tabWidget->currentIndex() == Mode::DMENU)
        searchDmenu(arg1);
    else
        searchApplication(arg1);
}

void MainWindow::on_programList_itemClicked(QListWidgetItem *item)
{
    QProcess::startDetached(item->text());
    this->hide();
}

void MainWindow::on_applicationsList_itemClicked(QListWidgetItem *item)
{
    QProcess::startDetached(item->data(Qt::UserRole).toString());
    this->hide();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    this->hide();
    //event->ignore();
    event->accept();
}

void MainWindow::on_switchTabVisibility_clicked()
{
    tabSwitchVisibility();
}
